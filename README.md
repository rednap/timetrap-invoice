# Timetrap Invoice

Python tool that exports/archives timetrap time sheets into an html invoice. zip the output and unzip on a webserver, or drop it in an s3 bucket and send the link to your client.

### Prerequisites

Before you install, ensure you have [python3](https://www.python.org/downloads/), and timetrap:
[Timetrap: Simple command line timetracker](https://github.com/samg/timetrap)
Other than that, basic knowledge of html, js, python, and the unix terminal are helpful.

Then, clone the repo:

```
 $ git clone https://rednap@bitbucket.org/rednap/timetrap-invoice.git timetrap-invoice
```

### Config

Change into the project directory:

```
 $ cd timetrap-invoice
```

Edit the py file with your editor of choice, and edit the config variables (lines 13-18):

```
 12 #config vars below
 13 yourdomain = "http://www.yourdomain.com/"
 14 organization = "Your Name or Business"
 15 workdescrip = "work description"
 16 phonenumber = "(555)555-5555"
 17 emailaddress = "name@yourdomain.com"
 18 orgimage = "https://s3.amazonaws.com/believe-it-or-not-im-walking-on-air/starry-eyed-man.jpg"
```

### Installing

Once the py file is configured, simply copy into a directory in your PATH. (/usr/local/bin/ is just a suggestion)

```
 $ cp invoice.py /usr/local/bin/invoice
```

Finally, make the new file executable, and confirm it's in your PATH

```
 $ chmod 777 /usr/local/bin/invoice
 $ which invoice
```

### Basic Usage

```
 $ mkdir new-invoice && cd new-invoice
 $ invoice sheet-name 99.99
```

where "sheet-name" is the name of the timetrap sheet, and "99.99" is the rate per hour. if everything is set up right you will see it export/create some files:

```
 $ invoice sheet-name 99.99
 # t display sheet-name -f csv > sheet-name-2019-03-09-15-54.csv
 # t display sheet-name -f json > sheet-name-2019-03-09-15-54.json
 # t display sheet-name > sheet-name-2019-03-09-15-54.txt
 # writing sheet-name-2019-03-09-15-54.html
 # writing index.html
 Archive 14 entries?
```

At this point, you can choose whether you archive the timetrap sheet or not with "yes" or "no" (also: "y" or "n"), and that's it.

## Deployment

Simply upload the files to your webserver, and send the link.

## Built With

* [Python](https://www.python.org/) - Everyone's favorite!
* [Timetrap](https://github.com/samg/timetrap) - Simple command line timetracker
* [Materialize](https://materializecss.com/) - A modern responsive front-end framework based on Material Design
* [JQuery](https://jquery.com/) - Fully featured JavaScript library

## Authors

* **Andy Klier** - *Initial work* - [http://www.andyklier.com/](http://www.andyklier.com/)
