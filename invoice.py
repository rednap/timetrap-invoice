#!/usr/bin/env python
import os
import datetime
import sys
import csv
import time
from zipfile import ZipFile

now = datetime.datetime.now()
nowstr = now.strftime("%Y-%m-%d-%H-%M")
sheet = sys.argv[1]
rate = sys.argv[2]

#config vars below
yourdomain = "http://www.yourdomain.com/"
organization = "Your Name or Business"
workdescrip = "work description"
phonenumber = "(555)555-5555"
emailaddress = "name@yourdomain.com"
orgimage = "https://s3.amazonaws.com/believe-it-or-not-im-walking-on-air/starry-eyed-man.jpg"

html = """<!DOCTYPE html>
<html>
<head>
    <title>Invoice</title>
    <!--Import Google Icon Font-->
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Compiled and minifed jQuery -->   
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <!-- Compiled and minified CSS -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css">
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
    <!--Let browser know website is optimized for mobile-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <style>    
        /* my styles */
        .redtext {{color:#ff0000;}}
        .brbr {{margin-bottom:14px;}}
        .margin_lr_10 {{
            margin-left:10px;
            margin-right:10px;
        }}
        .h1name {{
            font-size:1.4em;
            margin:0;
        }}
        .h2lowlytitle {{
            font-size:1.1em;
            margin:0;
            color:#1565c0;
        }}
        .subinfo {{
            margin-top:14px;
            font-size:.8em;
        }}
        .subinfo a {{
            color:#666666;
        }}
        .subinfo a:hover {{
            text-decoration:underline;
        }}
        .jsonholder {{
            margin-top:14px;
        }}
        .datetime {{
            font-size:.8em;
            white-space:nowrap;
            margin:0;
            padding-right:3px;
        }}
        .note {{
            margin:0;
        }}
        .notelink {{
            font-size:.8em;
            color:#79a6d2;
            text-decoration:underline;
        }}
        .notelink:hover {{
            text-decoration:none;
        }}
        .thead {{
            display:none;
        }}
        .totalhours {{
            margin:0;
            font-size:1.3em;
            display:none;
        }}
        .price {{
            display:none;
            margin-top:6px;
            margin-bottom:10px;
            font-size:3.1em;
        }}
        .hourly {{
            display:none;
            font-size:1.1em;
        }}
        .downloadzip {{
            margin-bottom:20px;
        }}
        @media screen and (max-width: 993px) {{
            /*ipad*/
        }}
        @media screen and (max-width: 700px) {{
            /* small screens */
            .container {{
                width:100%;
            }}
        }}
        @media screen and (max-width: 400px) {{
            /* small screens */
        }}
    </style>
</head>
<body class="grey lighten-3">
    <div class="container white" style="height:100%;">
        <nav class="grey darken-3" id="navigation">
            <div class="nav-wrapper">
                <a href="{yourdomain}" target="_blank" class="brand-logo center"><i class="material-icons" style="font-size:4rem">accessibility</i></a>
            </div>
        </nav>
        <ul>
            <li>
                <div class="margin_lr_10" style="height:100%;">
                    <h1 class="h1name">{organization}</h1>
                    <h2 class="h2lowlytitle">{workdescrip}</h2>

                    <div class="subinfo">
                        <a href="tel:+{phonenumber_stripped}">{phonenumber}<a/><br>
                        <a href="mailto:{emailaddress}">{emailaddress}</a><br>
                    </div>
                    <div class="jsonholder">
                    <div id="loading">... Loading JSON from file ...</div>
                    <h3 class="totalhours"></h3>
                    <h2 class="price"></h1><span class="hourly"></span>
                    <a href="invoice.zip"><div class="downloadzip">[ Download ZIP ]</div></a>
                    <table id="jsontable" class="highlight responsive-table">
                        <thead class="thead" style="diplay:none">
                            <tr class="thead">
                                <th class="thead">Note</th>
                                <th class="thead">Start</th>
                                <th class="thead">End</ht>
                            </tr>
                        </thead>
                    </table>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <script>
$(document).ready(function(){{
    /* json from file */
    var filename;
    if (document.location.href.match(/[^\/]+$/) === null) {{
        filename = "index.html";
    }} else {{
        filename = document.location.href.match(/[^\/]+$/)[0];
    }}
    filename = filename.split('.');
    filenamejson = filename[0]+".json";
    filenametxt = filename[0]+".txt";
    // MY RATE
    rate = {jobrate};
    function createDate(datetime) {{
        datetime = datetime.split(' ');
        datetime = datetime[0]+" "+datetime[1];
        match = datetime.match(/^(\d+)-(\d+)-(\d+) (\d+)\:(\d+)\:(\d+)$/);
        datetime = new Date(match[1], match[2] - 1, match[3], match[4], match[5], match[6]);
        var month = datetime.getUTCMonth() + 1;
        var day = datetime.getUTCDate();
        var year = datetime.getUTCFullYear();
        var hours = datetime.getHours();
        var ampm;
        if (hours > 12) {{
            ampm = "PM";
            hours = hours - 12;
        }} else if (hours == 12) {{
            ampm = "PM";
        }} else {{
            ampm = "AM";
        }}
        var minutes = datetime.getMinutes();
        if (minutes < 10) {{
            minutes = "0"+minutes;
        }}
        newdate = month+"/"+day+"/"+year+" "+hours+":"+minutes+ampm;
        return newdate;
    }}
    function getTotal(file) {{
        $.get(file, function( data ) {{
            var text = data;
            text = text.replace(/ +(?= )/g,'');
            text = text.split(' ');
            var index = text.indexOf("Total");
            index++;
            var totalhours = text[index];
            totalhours = totalhours.split(":");
            hours = totalhours[0];
            minutes = totalhours[1];
            mdec = minutes / 60;
            total = hours*rate;
            total = total + (mdec*rate);
            total = total.toFixed(2);
            total = "$"+total;
            rate = " at $"+rate+"/hr";
            totalhours = hours+":"+minutes;
            $(".totalhours").html("Total Hours: "+totalhours);
            $(".totalhours").show();
            $(".price").html(total);
            $(".price").css('display', 'inline-block');
            $(".hourly").html(rate);
            $(".hourly").show();
        }});
    }}
    setTimeout(function(){{ 
        $.getJSON(filenamejson, function(json) {{
            $("#loading").hide();
            $(".thead").show();
            getTotal(filenametxt);
            var tbl_body = document.createElement("tbody");
            $.each(json, function() {{
                var tbl_row = tbl_body.insertRow();
                var newlink = {{}};
                $.each(this, function(k , v) {{
                    if (k != "id" && k != "sheet") {{
                        var cell = tbl_row.insertCell();
                        if (k != "note") {{
                            cell.className = 'datetime';
                            v = createDate(v);
                        }} else {{
                            if (v.indexOf("[http") > -1 && v.indexOf("]") > -1) {{
                                var filler_pieces = v.split("[");
                                var arrayLength = filler_pieces.length;
                                var x = 0;
                                for (var i = 0; i < arrayLength; i++) {{
                                    if (filler_pieces[i].indexOf("]") > -1) {{
                                        var middle = filler_pieces[i].substring(0, filler_pieces[i].indexOf("]"));
                                        var pieces = middle.split("/");
                                        var pp = pieces[2].split(".");
                                        var innerhtml = pp[pp.length-2]+"."+pp[pp.length-1];
                                        var nl = document.createElement('a');
                                        nl.setAttribute('href', middle);
                                        nl.setAttribute('target', '_blank');
                                        nl.setAttribute('class', 'notelink');
                                        nl.innerHTML = innerhtml;
                                        newlink[x] = nl
                                        v = v.replace("["+middle+"]", "");
                                        x+=1;
                                    }}
                                }}
                            }}
                            if (v == "") {{
                                v = "*no notes*";
                            }}
                        }}
                        cell.appendChild(document.createTextNode(v));
                        if (Object.keys(newlink).length !== 0) {{
                            const values = Object.values(newlink);
                            for (const value of values) {{
                                cell.appendChild(document.createElement('br'));
                                cell.appendChild(value);
                            }}
                        }}
                        newlink = {{}};
                    }}
                }})
            }})
            $("#jsontable").append(tbl_body);
        }});
    }}, 1000);
}});
    </script>
</body>
</html>"""

indexhtml = """<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta http-equiv="refresh" content="1;url={refreshhtml}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>{organization}</title>
    <link rel="stylesheet" href="style.css" type="text/css">
    <link rel="SHORTCUT ICON" href="favicon.ico">
    <style>
    html, body {{
        margin: 0;
        padding: 0;
        background-color:#0F2228;
        font-family:Arial;
        font-size:1em;
    }}
    img {{
        border:0;
    }}
    table {{
        border-collapse: collapse;
    }}
    table tr {{
        vertical-align: top;
    }}
    table td,
    table th {{
        padding: 0;
    }}
    a:link {{ color:#852218; text-decoration:none; }}
    a:visited {{ color:#852218; text-decoration:none; }}
    a:hover {{ color:#852218; text-decoration:underline; }}
    a:active {{ color:#852218; text-decoration:none; }}
    #me {{
        width:200px;
        height:208px;
        position:absolute;
        top:40%;
        left:50%;
        margin:-50px auto auto -100px;
        border:0;
        text-align:center;
    }}
    </style>
</head>
<body>
    <div id="me">
        <a href="{refreshhtml}"><img src="{orgimage}" alt="me, myself, and i"></a>
    </div>
</body>
</html>"""

def get_all_file_paths(directory):
    # initializing empty file paths list
    file_paths = []

    # crawling through directory and subdirectories
    for root, directories, files in os.walk(directory):
        for filename in files:
            # join the two strings in order to form the full filepath.
            filepath = os.path.join(root, filename)
            file_paths.append(filepath)

    # returning all file paths
    return file_paths

def invoice(sheet, nowstr, rate) :
    ''' create invoices with sheet '''
    # create csv to read
    csvfn = sheet+"-"+nowstr+".csv"
    cmd = "t display "+sheet+" -f csv > "+csvfn
    os.system(cmd)
    print("# "+cmd)
    # read csv to get start and end
    with open(sheet+'-'+nowstr+'.csv') as csvfile:
        reader = csv.DictReader(csvfile)
        x = 0;
        for row in reader:
            end = row['start']
            endarr = end.split(' ')
            end = endarr[0]
            if (x == 0) :
                start = row['start']
                startarr = start.split(' ')
                start = startarr[0]
            x += 1;
    # create json
    jsonfn = sheet+"-"+nowstr+".json"
    cmd = "t display "+sheet+" -f json > "+jsonfn
    os.system(cmd)
    print("# "+cmd)
    # create txt
    txtfn = sheet+"-"+nowstr+".txt"
    cmd = "t display "+sheet+" > "+txtfn
    os.system(cmd)
    print("# "+cmd)
    # create html
    htmlfn = sheet+"-"+nowstr+".html"
    f = open(htmlfn,"w+")
    numlist = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    phonenumber_stripped = ""
    notint = False
    for c in phonenumber:
        try:
            val = int(c)
        except ValueError:
            notint = True
        if not notint and int(c) in numlist:
            phonenumber_stripped += c
        notint = False
    newhtml = html.format(jobrate=rate, yourdomain=yourdomain, organization=organization, workdescrip=workdescrip, phonenumber=phonenumber, phonenumber_stripped=phonenumber_stripped, emailaddress=emailaddress)
    f.write(newhtml)
    f.close
    print("# writing "+htmlfn)
    # create index.html
    indexhtmlfn = "index.html"
    f = open(indexhtmlfn,"w")
    newindexhtml = indexhtml.format(refreshhtml=htmlfn, organization=organization, orgimage=orgimage)
    f.write(newindexhtml)
    f.close()
    print("# writing "+indexhtmlfn)
    # archive sheet
    cmd = "t archive "+sheet+" --start \""+start+"\" --end \""+end+"\""
    os.system(cmd)
    print("# "+cmd)
    # zip files!
    directory = './'
    while not '</html>' in open(indexhtmlfn).read():
        time.sleep(1)
    # calling function to get all file paths in the directory
    file_paths = get_all_file_paths(directory)

    # printing the list of all files to be zipped
    print('Following files will be zipped:')
    for file_name in file_paths:
        print(file_name)

    # writing files to a zipfile
    with ZipFile('invoice.zip','w') as zip:
        # writing each file one by one
        for file in file_paths:
            zip.write(file)
    return;

invoice(sheet, nowstr, rate)
